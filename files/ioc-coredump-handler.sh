#!/bin/sh

pid=${1}
usr=${2}
grp=${3}

cwd=/proc/${pid}/cwd
exe=`readlink /proc/${pid}/exe`
exe=`basename ${exe}`

if [ "${exe}x" = "softIocx" -o "${exe}x" = "softIocPVAx" ]; then
  corename="core"
else
  corename="core.${pid}"
fi

rm -f ${cwd}/${corename}
cat > ${cwd}/${corename}
chown ${usr}:${grp} ${cwd}/${corename}
