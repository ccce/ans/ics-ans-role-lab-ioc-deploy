# ics-ans-role-lab-ioc-deploy

Ansible role to deploy IOCs. Does not work with ansible inventories but requires the CCCE back-end to pass lists of IOCs to deploy and/or undeploy.

Will clone a specific reference of an IOC repository to a host and will run this as a system daemon in a procServ container. Does a lot of other modifications to configure the IOCs and associated tools, and requires the host to be a so-called "IOC host" (configurable in CSEntry upon registration).

## Role Variables

```yaml
---
ioc_packages:
  - git
  - procServ
  - logrotate
  - nfs-utils
  - "@Development Tools"
  - tclx
  - which

# ioc user variables
ioc_user_name: iocuser
ioc_user_id: "1042"
ioc_group_name: iocgroup
ioc_group_id: "1042"

# channel access port
ioc_ca_port: 5064

# where ioc source code will be cloned
ioc_iocs_directory: /opt/iocs

# where conda environments will be created
ioc_conda_envs_directory: /home/{{ ioc_user_name }}/.conda/envs

# Variable can be used to add extra packages for development
ioc_dev_packages: []

# hostname of the nonvolatile server
# If not empty, the directory /export/nonvolatile/{{ ioc.name }} will be created
# on that machine and will be mounted locally
# We use "" string by default so that it's easy to disable at host level
ioc_nonvolatile_server: ""
# Local nonvolatile base directory
ioc_nonvolatile_path: /opt/nonvolatile
# Remote nonvolatile base directory
ioc_nonvolatile_server_path: "/export/nonvolatile"

# The following variables can be set to override the defaults from e3-common
# module:
# ioc_log_server_name: localhost
# ioc_errorlog_server_port: "9001"
# ioc_caputlog_server_port: "9001"
# ioc_asg_filename: "unrestricted_access.acf"
# ioc_facility_name: ""

# e3 nfs mounts
ioc_nfs_mountpoints:
  - { src: "e3-share-lab.cslab.esss.lu.se:/e3", path: "/epics"}

# default epics share to use
ioc_epics_root: /epics

# default ioc options
ioc_ioc_defaults:
  epics_root: "{{ ioc_epics_root }}"
  start: true
  enable: true

# default ioc.yml options
default_ioc_yml:
  address_list: []
  packages: []
  realtime: false
```

## License

BSD 2-clause.
