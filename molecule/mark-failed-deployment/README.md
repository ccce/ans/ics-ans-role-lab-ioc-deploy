# mark-failed-deployment

This scenario tests that a failed IOC deployment will be retried the next time the job runs; even if the IOC hasn't changed and deployment wasn't forced.
