import os

import testinfra.utils.ansible_runner
import yaml

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("molecule_group")


def test_failed_at_the_same_task(host):
    first_ioc = "/tmp/ioc_failure"
    second_ioc = "/tmp/second_ioc_failure"

    assert host.file(first_ioc).is_file
    assert host.file(second_ioc).is_file

    first_iocs_deployed = yaml.safe_load(host.file(first_ioc).content_string)[
        "iocs_deployed"
    ]
    assert list(first_iocs_deployed.keys()) == ["test-no-epics-version"]
    first_ioc_result = first_iocs_deployed["test-no-epics-version"]

    assert not first_ioc_result["successful"]

    second_iocs_deployed = yaml.safe_load(host.file(second_ioc).content_string)[
        "iocs_deployed"
    ]
    assert first_iocs_deployed == second_iocs_deployed
