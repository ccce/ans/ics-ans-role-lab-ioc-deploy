require essioc

iocshLoad("$(essioc_DIR)/common_config.iocsh")

## For commands to be run after iocInit, use the function afterInit()

iocInit()

date
