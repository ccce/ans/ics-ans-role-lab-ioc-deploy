# dont-stop-deployment

This scenario tests that failing to deploy one IOC will not prevent the deployment of the other IOCs in the inventory. The role shall still fail but only as the last step
