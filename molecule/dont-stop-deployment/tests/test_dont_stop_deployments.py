import os

import testinfra.utils.ansible_runner
import yaml

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("molecule_group")


def test_last_ioc_is_deployed(host):
    # The deployment scenario is set up in a way that
    #  only the last IOC can be deployed; all the previous ones have issues
    assert host.service("ioc@test-correct-ioc").is_running


def test_ioc_deployment_result(host):
    ioc_deployment_result = "/tmp/ioc_deployment_result"

    assert host.file(ioc_deployment_result).is_file

    ioc_deployment_result = yaml.safe_load(
        host.file(ioc_deployment_result).content_string
    )

    iocs_deployed = ioc_deployment_result["iocs_deployed"]

    # ioc_deployed should have 5 entries; one for each IOC
    assert len(iocs_deployed.keys()) == 4

    # test-correct-ioc should be successful
    test_correct_ioc = iocs_deployed["test-correct-ioc"]
    assert test_correct_ioc["successful"]

    # test-conda should not be successful
    test_conda = iocs_deployed["test-conda"]
    assert not test_conda["successful"]
    assert (
        '  "error": "OSError(5)",'
        in test_conda["ansible_failed_result"]["stdout_lines"]
    )

    # test-no-epics-version should not be successful
    test_no_epics_version = iocs_deployed["test-no-epics-version"]
    assert not test_no_epics_version["successful"]
    assert test_no_epics_version["ansible_failed_result"]
    assert (
        test_no_epics_version["ansible_failed_result"]["assertion"]
        == "_ioc_metadata.epics_version is defined"
    )

    # test-no-require-version should not be successful
    test_no_require_version = iocs_deployed["test-no-require-version"]
    assert not test_no_require_version["successful"]
    assert test_no_require_version["ansible_failed_result"]
    assert (
        test_no_require_version["ansible_failed_result"]["assertion"]
        == "_ioc_metadata.require_version is defined"
    )
