import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("nonvolatile")


def test_nonvolatile(host):
    test_nfs_dir = "/export/nonvolatile/CCCE_test-nfs"
    assert host.file(test_nfs_dir).exists
    assert host.file(test_nfs_dir).is_directory
    # Test that our modification was not undone by IOC deploy
    assert host.file(test_nfs_dir).mode == 0o744
