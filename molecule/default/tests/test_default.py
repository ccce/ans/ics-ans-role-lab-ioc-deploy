import os
import re
import stat
import time

import pytest
import testinfra.utils.ansible_runner
import yaml

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("molecule_group")

DEPLOYED_IOCS = [
    "CCCE:test-nfs",
    "CCCE:test-conda",
    "CCCE:test-realtime",
    "CCCE:test_packages_nfs_ioc",
    "CCCE:no_address_list",
    "CCCE:test-with-tag",
]

UNDEPLOYED_IOCS = ["CCCE:will-be-undeployed", "CCCE:test-conda-remove"]

CONDA_ENV_PATH = "/home/iocuser/.conda/envs/"

CAGET = "/tmp/caget"
CAPUT = "/tmp/caput"

IOC_EXPORTER_PORT = "12110"

IOC_NAME_PATTERN = re.compile("[^a-zA-Z0-9_-]")
IOC_NAME_REPLACE = "_"


def sluggify(ioc):
    return IOC_NAME_PATTERN.sub(IOC_NAME_REPLACE, ioc)


def systemd_override_dir(ioc_slug):
    return f"/etc/systemd/system/ioc@{ioc_slug}.service.d"


def host_caget(host, pv):
    return host.run(f"{CAGET} -w 5 -t {pv}")


def host_caput(host, pv, val):
    return host.run(f"{CAPUT} -w 5 {pv} {val}")


@pytest.mark.parametrize("iocname", UNDEPLOYED_IOCS)
def test_ioc_was_undeployed(host, iocname):
    opt_iocs = "/opt/iocs"
    opt_nonv = "/opt/nonvolatile"
    iocslug = sluggify(iocname)

    assert not host.file(systemd_override_dir(iocslug)).exists
    assert not host.file(os.path.join(opt_iocs, iocslug)).exists
    assert not host.file(os.path.join(opt_nonv, iocslug)).exists

    # Logfiles should still be there. conserver log is only created when someone
    # connects to the console so we cannot rely on its existence
    assert host.file(f"/var/log/procServ/out-{iocslug}.log").is_file

    # Console should no longer be configured
    cmd = host.run(f"console -I {iocslug}")
    assert f"console `{iocslug}' not found" in cmd.stdout

    ioc_deployment_result = yaml.safe_load(
        host.file("/tmp/ioc_deployment_result").content_string
    )

    # Proof that the now undeployed IOC was in fact deployed
    iocs_deployed = ioc_deployment_result["iocs_deployed"]
    assert iocs_deployed[iocname]["successful"]

    ioc_undeployment_result = yaml.safe_load(
        host.file("/tmp/ioc_deployment_side_effect_result").content_string
    )

    iocs_undeployed = ioc_undeployment_result["iocs_undeployed"]
    assert iocs_undeployed[iocname]["successful"]


def test_list_of_undeployed_iocs(host):
    ioc_deployment_result = yaml.safe_load(
        host.file("/tmp/ioc_deployment_side_effect_result").content_string
    )

    iocs_undeployed = ioc_deployment_result["iocs_undeployed"]

    # Only these two IOCs should have been undeployed
    assert set(UNDEPLOYED_IOCS) == set(iocs_undeployed.keys())


@pytest.mark.parametrize("iocname", DEPLOYED_IOCS)
def test_ioc_was_deployed(host, iocname):
    opt_iocs = "/opt/iocs"
    iocname = sluggify(iocname)
    assert host.file(systemd_override_dir(iocname)).is_directory
    assert host.file(os.path.join(opt_iocs, iocname)).is_directory


@pytest.mark.parametrize("iocname", DEPLOYED_IOCS)
def test_nfs_nonvolatile(host, iocname):
    mount_source = "nonvolatile:/export/nonvolatile/"
    iocname = sluggify(iocname)
    opt_nonv = os.path.join("/opt/nonvolatile", iocname)
    assert host.file(opt_nonv).exists
    assert host.file(opt_nonv).is_directory

    assert host.mount_point(opt_nonv).exists
    assert host.mount_point(opt_nonv).filesystem == "nfs4"
    assert host.mount_point(opt_nonv).device == os.path.join(mount_source, iocname)

    # Check that mode was not reverted for "CCCE_test-nfs" in side-effect
    if iocname == "CCCE_test-nfs":
        mode = 0o744
    else:
        mode = 0o755
    assert host.file(opt_nonv).mode == mode


def test_gcc_is_installed(host):
    gcc = host.package("gcc")
    assert gcc.is_installed


def test_installed_package(host):
    image_magick = host.package("ImageMagick-devel")
    assert image_magick.is_installed


@pytest.mark.parametrize("iocname", DEPLOYED_IOCS)
def test_address_list(host, iocname):
    sluggified_iocname = sluggify(iocname)
    ioc_log = host.file(f"/var/log/procServ/out-{sluggified_iocname}.log")
    ca_addr_list = (
        "127.0.0.1 0.0.0.0" if iocname == "CCCE:test_packages_nfs_ioc" else ""
    )
    assert ioc_log.contains(rf'EPICS_CA_ADDR_LIST\s*=\s*"{ca_addr_list}"')


def test_realtime_scheduling(host):
    iocname = "CCCE:test-realtime"

    iocsh_bash = host.process.filter(comm="iocsh.bash")
    for proc in iocsh_bash:
        args = proc["args"]
        if sluggify(iocname) in args:
            assert "--realtime" in args
            break
        else:
            assert "--realtime" not in args

    pid = host_caget(host, f"{iocname}:PROCESS_ID")
    assert pid.rc == 0
    pid = int(pid.stdout)

    softIoc = host.process.filter(comm="softIoc")
    softIoc.extend(host.process.filter(comm="softIocPVA"))
    # Line we are interested in looks like this:
    #  'VmLck:      0 kB'
    # Get number after "VmLck:" and before " kB"
    vmlck_re = re.compile(r"^VmLck:\s*(\d+)\s*kB", re.MULTILINE)
    # Line we are interested in looks like this:
    #  'CapPrm: 0000000000000000'
    capprm_re = re.compile(r"^CapPrm:\s*(\d+)", re.MULTILINE)
    for proc in softIoc:
        pid_stat = host.file(f"/proc/{proc.pid}/status")
        assert pid_stat.exists
        # Get the size of locked memory:
        vmlck = vmlck_re.search(pid_stat.content_string).group(1)
        # Get permitted capabilities:
        capprm = capprm_re.search(pid_stat.content_string).group(1)
        if pid == proc.pid:
            # Scheduling policy; FF -- SCHED_FIFO,  RR - SCHED_RR
            assert proc.policy in ["FF", "RR"]
            # Real-time scheduling priority, a number in the range 1 to 99 for
            # processes scheduled under a real-time policy, or 0, for non-real-time
            # processes
            assert proc.rtprio > 0
            # From man 5 proc:
            # For processes running a real-time scheduling policy this is the negated
            # scheduling priority, minus one; that is, a number in the range -2 to -100,
            # corresponding to real-time priorities 1 to 99.
            # For processes running under a non-real-time scheduling policy, this is the
            # raw nice value as represented in the kernel. The kernel stores nice values
            # as numbers in the range 0 (high) to 39 (low), corresponding to the user-
            # visible nice range of -20 to 19.
            # BUT: testinfra's Process uses `ps` to get the priority information, so
            # priority above 39 is realtime prio
            assert proc.pri > 39
            # Assert that there is memory locked
            assert int(vmlck) > 0
            # Decode the capability mask
            cmd = host.run(f"/usr/sbin/capsh --decode={capprm}")
            assert cmd.succeeded
            assert "cap_sys_nice" in cmd.stdout
            assert "cap_ipc_lock" in cmd.stdout
        else:
            assert proc.policy == "TS"
            assert proc.pri < 40
            assert proc.rtprio == "-"
            # Assert that there is no memory locked
            assert int(vmlck) == 0
            # Assert that the process has no permitted capabilities
            assert capprm == "0000000000000000"


def check_permissions(fl, is_dir=False):
    assert fl.exists
    if is_dir:
        assert fl.is_directory
        assert fl.mode == 0o0755
    else:
        assert fl.is_file
        assert fl.mode == 0o0644
    assert fl.user == "iocuser"
    assert fl.group == "iocgroup"


def test_procserv(host):
    procserv = host.package("procServ")
    assert procserv.is_installed

    procserv = host.file("/var/log/procServ")
    check_permissions(procserv, True)
    for f in procserv.listdir():
        check_permissions(host.file(os.path.join("/var/log/procServ", f)))


def test_iocuser_and_iocgroup_exist(host):
    iocuser = host.user("iocuser")
    iocgroup = host.group("iocgroup")
    assert iocuser.exists
    assert iocgroup.exists
    assert iocgroup.gid in iocuser.gids


def test_iocuser_in_realtime_group(host):
    realtime = host.group("realtime")
    assert realtime.exists
    assert realtime.gid in host.user("iocuser").gids


def test_prometheus_ioc_exporter_is_running(host):
    service = host.service("ioc-exporter")
    assert service.is_running


@pytest.mark.parametrize(
    "iocname",
    [
        "CCCE:test-nfs",
        "CCCE:test-conda",
        "CCCE:test-realtime",
        "CCCE:test_packages_nfs_ioc",
        "CCCE:no_address_list",
    ],
)
def test_prometheus_ioc_exporter_reports_correct_on_unchanged_repos(host, iocname):
    cmd = host.command(f"curl localhost:{IOC_EXPORTER_PORT}")
    assert f'ioc_repository_dirty{{ioc="{iocname}"}} 0.0' in cmd.stdout
    assert f'ioc_repository_local_commits{{ioc="{iocname}"}} 0.0' in cmd.stdout
    assert f'ioc_service_disabled{{ioc="{iocname}"}} 0.0' in cmd.stdout
    assert (
        f'ioc_essioc_missing{{ioc="{iocname}"}}' in cmd.stdout
    )  # we do not care about result since usage varies for test IOCs


@pytest.mark.parametrize("iocname", DEPLOYED_IOCS)
def test_ioc_service_started(host, iocname):
    sluggified_iocname = sluggify(iocname)
    ioc_service = host.service(f"ioc@{sluggified_iocname}")
    assert ioc_service.is_running == (iocname in DEPLOYED_IOCS)


@pytest.mark.parametrize("iocname", DEPLOYED_IOCS)
def test_ioc_service_enabled(host, iocname):
    sluggifiediocname = sluggify(iocname)
    ioc_service = host.service(f"ioc@{sluggifiediocname}")
    assert ioc_service.is_enabled == (iocname in DEPLOYED_IOCS)


@pytest.mark.parametrize("iocname", DEPLOYED_IOCS)
def test_ioc_console_configured(host, iocname):
    iocname = sluggify(iocname)
    # -I is the same as -i but acts only on the primary server
    cmd = host.run(f"console -I {iocname}")
    """
      The -i option outputs status information regarding each console in 15
      colon-separated fields. https://linux.die.net/man/1/console
    """
    assert cmd.succeeded

    (
        console_name,
        console_hostname_pid_socket,
        console_type,
        console_details,
        console_users_list,
        console_state,
        console_perm,
        console_logfile_details,
        console_break_seq,
        console_reup,
        console_aliases,
        console_options,
        console_initcmd,
        console_idletimeout,
        console_idlestring,
    ) = cmd.stdout.split(":")

    assert console_name == f"{iocname}"

    # Check that it is UDS; `%' represents a Unix domain socket
    assert console_type == "%"

    # Check the UDS location
    assert console_details.startswith(f"/run/ioc@{iocname}/control,")

    # Check logfile details
    log = console_logfile_details.split(",")

    logfile = f"/var/log/conserver/console-{iocname}.log"
    assert logfile in log

    # activity logging
    assert "act" in log

    # timestamp interval
    assert "{}".format(24 * 60 * 60) in log

    assert console_reup == "noautoup"

    # Check that only "ondemand" and "login" are set
    assert set(["ondemand", "login"]) == set(console_options.split(","))


@pytest.mark.parametrize("iocname", DEPLOYED_IOCS)
def test_ioc_pv_reachable(host, iocname):
    cmd = host_caget(host, f"{iocname}:BaseVersion")
    assert cmd.succeeded


def test_epics_version_pv(host):
    iocname = "CCCE:test-nfs"
    cmd = host_caget(host, f"{iocname}:BaseVersion")
    assert cmd.succeeded
    assert "7.0.6.1" in cmd.stdout


def test_require_version_pv(host):
    iocname = "CCCE:test-nfs"
    cmd = host_caget(host, f"{iocname}:require_VER")
    assert cmd.succeeded
    assert "4.0.0" in cmd.stdout


def test_require_5_version_pv(host):
    iocname = "CCCE:no_address_list"
    cmd = host_caget(host, f"{iocname}:requireVersion")
    assert cmd.succeeded
    assert "5.0.0" in cmd.stdout


def test_e3_version_paths_in_startup_script(host):
    iocname, e3_version = "CCCE:test-nfs", "/epics/base-7.0.6.1/require/4.0.0"
    iocname = sluggify(iocname)
    start_ioc = f"/opt/iocs/{iocname}/.deployment/start_ioc.sh"
    content = host.file(start_ioc).content_string
    assert e3_version in content


def test_ansible_galaxy_present(host):
    # Test for some packages and repos that should be installed by galaxy roles
    cmd = host.run("yum repolist")
    assert "epel-ess/x86_64" in cmd.stdout
    assert host.package("conda")
    assert host.package("conserver")
    assert host.package("conserver-client")


def test_os_release(host):
    assert host.file("/etc/os-release").contains("CentOS")


def test_nfs_mounts_exist(host):
    assert host.mount_point("/epics")


def test_non_volatile_writable(host):
    assert host.file("/opt/nonvolatile/").mode & stat.S_IWUSR


def test_ioc_deployment_result(host):
    ioc_deployment_result = "/tmp/ioc_deployment_result"

    assert host.file(ioc_deployment_result).exists
    assert host.file(ioc_deployment_result).is_file

    ioc_deployment_result = yaml.safe_load(
        host.file(ioc_deployment_result).content_string
    )

    awx_vars = ioc_deployment_result["awx"]
    assert awx_vars["awx_job_id"] == "foo"

    iocs_deployed = ioc_deployment_result["iocs_deployed"]
    for ioc in DEPLOYED_IOCS:
        assert iocs_deployed[ioc]["successful"]

    # Make sure that only the IOCs we wanted to deploy are in the dictionary
    deployed_iocs_set = set(DEPLOYED_IOCS)
    deployed_iocs_set.update(UNDEPLOYED_IOCS)
    assert deployed_iocs_set == iocs_deployed.keys()


def get_pid_pair(host, iocname):
    old_pid = host.file(f"/tmp/{iocname}_pid").content_string
    cmd = host_caget(host, f"{iocname}:PROCESS_ID")
    assert cmd.succeeded
    pid = int(cmd.stdout)

    return (old_pid, pid)


def test_if_tag_to_same_commit_restarts_ioc(host):
    # This IOC is redeployed with a tag during side_effect
    iocname = "CCCE:test-with-tag"
    old_pid, pid = get_pid_pair(host, iocname)
    assert int(old_pid) != int(pid)


def test_that_iocs_are_not_restarted_on_unchanged_deploy(host):
    # This IOC is redeployed identically during side_effect
    iocname = "CCCE:test-nfs"
    old_pid, pid = get_pid_pair(host, iocname)
    assert int(old_pid) == int(pid)


def test_coredump(host):
    iocname = "CCCE:test_packages_nfs_ioc"
    ioc_dir = f"/opt/iocs/{sluggify(iocname)}"
    core_file = f"{ioc_dir}/core"

    assert not host.file(core_file).exists

    # Get the PID of softIocPVA
    pid = host_caget(host, f"{iocname}:PROCESS_ID")
    assert pid.rc == 0
    # Cause a segmentation fault
    with host.sudo():
        host.command(f"kill -s SEGV {int(pid.stdout)}")

    # Give some time to dump core
    time.sleep(2)

    assert host.file(core_file).is_file
    assert host.run(f"cd {ioc_dir} && git diff --exit-code").succeeded


def test_preserve_tmp_e3(host):
    e3_private_dir = "/tmp/systemd-private-e3-iocsh-iocuser"
    assert host.file(e3_private_dir).exists
    assert host.file(e3_private_dir).is_directory

    # Run systemd-tmpfiles to clean /tmp
    cleanup = host.run("/tmp/cleanup")

    assert cleanup.succeeded
    ignore_line = f'Ignoring "{e3_private_dir}": a separate glob exists.'
    assert ignore_line in cleanup.stderr

    # Check that the individual files created by iocsh.bash were not even considered
    # First mention of "/tmp/systemd-private-e3-iocsh-iocuser":
    #  Ignoring "/tmp/systemd-private-e3-iocsh-iocuser": a separate glob exists.
    # Second mention of "/tmp/systemd-private-e3-iocsh-iocuser":
    #  Running clean action for entry x /tmp/systemd-private-e3-iocsh-iocuser
    mentions = list(filter(lambda x: e3_private_dir in x, cleanup.stderr.splitlines()))
    assert len(mentions) == 2

    assert host.file(e3_private_dir).exists


def test_location_pv_is_set_from_extra_vars(host):
    iocname = "CCCE:test_packages_nfs_ioc"
    cmd = host_caget(host, f"{iocname}:LOCATION")
    assert cmd.succeeded
    assert "git/CCCE_test_packages_nfs_ioc" in cmd.stdout


def is_item_in_env(host, iocname, env_item):
    cmd = host_caget(host, f"{iocname}:PROCESS_ID")
    assert cmd.succeeded
    pid = int(cmd.stdout)
    with host.sudo():
        environ = host.file(f"/proc/{pid}/environ")
        assert environ.exists
        return env_item in environ.content_string


def test_env_sh_ignored_for_require_4(host):
    # require-4 no longer automatically sources `env.sh` nor it allows specifying
    # a different environment file. So if the IOC uses require-4 we no longer source
    # `env.sh`
    iocname = "CCCE:test-realtime"
    assert not is_item_in_env(host, iocname, "IOC_SPECIFIC_ENV_VAR=")


def test_if_systemd_restarts_procserv(host):
    ioc = sluggify("CCCE:test-realtime")
    pid_re = re.compile(r"(?<=^pid:)\d+", re.MULTILINE)

    pid = pid_re.search(host.file(f"/run/ioc@{ioc}/info").content_string).group()
    assert host.process.get(pid=pid).comm == "procServ"

    # Kill procServ
    with host.sudo():
        host.run(f"kill {pid}")

    # Wait some time to make sure procServ was killed
    time.sleep(2)

    # Check that kill was successful
    try:
        host.process.get(pid=pid)
        msg = ""
    except RuntimeError as e:
        msg = str(e)

    assert msg == "No process found"

    # Give some time to systemd to restart and procServ to start
    time.sleep(2)

    new_pid = pid_re.search(host.file(f"/run/ioc@{ioc}/info").content_string).group()
    assert host.process.get(pid=new_pid).comm == "procServ"

    assert pid != new_pid


def test_conda_env_exists_for_deployed_ioc(host):
    iocname = sluggify("CCCE:test-conda")
    with host.sudo():
        assert host.file(os.path.join(CONDA_ENV_PATH, iocname)).exists
        assert host.file(os.path.join(CONDA_ENV_PATH, iocname)).is_directory


def test_conda_env_removed_for_deployed_ioc(host):
    iocname = sluggify("CCCE:test-conda-remove")
    with host.sudo():
        assert not host.file(os.path.join(CONDA_ENV_PATH, iocname)).exists
        assert not host.file(os.path.join(CONDA_ENV_PATH, iocname)).is_directory


def test_iocversion_is_exported_in_ioc_activate(host):
    opt_iocs = "/opt/iocs"
    iocname = sluggify("CCCE:test-nfs")
    iocversion = "master"
    ioc_activate_sh = os.path.join(opt_iocs, iocname, ".deployment", "ioc_activate.sh")

    assert host.file(ioc_activate_sh).contains(f'export IOCVERSION="{iocversion}"')
