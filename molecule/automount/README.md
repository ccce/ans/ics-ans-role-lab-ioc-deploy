# automount

This scenario tests that
*   the autosave and /epics (in case of NFS IOCs) shares are automatically mounted when the systemd service is started and if mounting any of them fails the IOC is not started and the service is marked as `failed`
*   the running indicator is created and removed
